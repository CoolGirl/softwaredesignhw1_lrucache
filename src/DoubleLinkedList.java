/**
 * Created by Евгения on 10.09.2016.
 */
class DoubleLinkedList<T> {

    private Node<T> head;
    private Node<T> tail;
    private final int capacity;
    private int currentSize;

    public DoubleLinkedList(int capacity) {
        assert capacity > 0;
        this.capacity = capacity;
    }

    public T pushHead(Node<T> newHead) {
        T removedValue = null;
        if (currentSize == capacity) {
            removedValue = removeBack();
        }
        if (head == null) {
            head = newHead;
            tail = newHead;
        } else {
            Node<T> oldHead = head;
            head = newHead;
            head.setNext(oldHead);
        }
        currentSize++;
        return removedValue;
    }

    public void moveToHead(Node<T> newHead) {
        boolean isHead = newHead == head;
        if (isHead) {
            return;
        }
        boolean isTail = newHead == tail;
        Node<T> prev = newHead.getPrev();
        if (isTail) {
            tail = prev;
            tail.setNext(null);
        } else {
            prev.setNext(newHead.getNext());
        }
        Node <T> oldHead = head;
        head = newHead;
        head.setNext(oldHead);
        head.setPrev(null);
    }

    private T removeBack() {
        T removedValue = tail.getValue();
        tail = tail.getPrev();
        if (tail != null) {
            tail.setNext(null);
        } else {
            head = null;
        }
        currentSize--;
        return removedValue;
    }

}
