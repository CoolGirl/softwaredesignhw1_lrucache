import java.util.HashMap;
import java.util.Map;

/**
 * Created by Евгения on 10.09.2016.
 */

//last recently used
//double linked list + hashmap
    /*Assertions, put, get*/
//Next task (hw2) is to write junit tests to this cache
public class LRUCache<K, V> {

    public final int cacheSize;

    public LRUCache(int cacheSize) {
        if (cacheSize <= 0){
            throw new IllegalArgumentException("Cache size must be positive.");
        }
        this.cacheSize = cacheSize;
        lruList = new DoubleLinkedList<Map.Entry<K, V>>(this.cacheSize);
    }

    private HashMap<K, Node<Map.Entry<K,V>>> nodeByKey = new HashMap<K, Node<Map.Entry<K,V>>>();
    private DoubleLinkedList<Map.Entry<K, V>> lruList;

    public V get(K key) {
        Node<Map.Entry<K, V>> requestedNode = nodeByKey.get(key);
        if (requestedNode != null) {
            lruList.moveToHead(requestedNode);
            return requestedNode.getValue().getValue();
        }
        return null;
    }

    public void put(final K key, final V value) {
        Node<Map.Entry<K,V>> newEntry = nodeByKey.get(key);
        if (newEntry == null) {
            newEntry = new Node<Map.Entry<K, V>>(new Map.Entry<K,V>(){

                @Override
                public K getKey() {
                    return key;
                }

                @Override
                public V getValue() {
                    return value;
                }

                @Override
                public V setValue(V value) {
                    throw new UnsupportedOperationException();
                }

            });
            nodeByKey.put(key, newEntry);
            Map.Entry<K,V> removedValue = lruList.pushHead(newEntry);
            if (removedValue != null) {
                nodeByKey.remove(removedValue.getKey());
            }
        }
        else {
            lruList.moveToHead(newEntry);
        }
    }


}
