/**
 * Created by Евгения on 10.09.2016.
 */
class Node<T> {

    private Node<T> prev;
    private Node<T> next;
    private T value;

    public Node(T value) {
        this.value = value;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }

    public void setPrev(Node<T> prev) {
        this.prev = prev;
    }

    public Node<T> getNext() {
        return next;
    }

    public Node<T> getPrev() {
        return prev;
    }

    public T getValue() {
        return value;
    }
}