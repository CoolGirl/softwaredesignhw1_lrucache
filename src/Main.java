/**
 * Created by Евгения on 23.09.2016.
 */
public class Main {

    public static void main(String [] args) {
        LRUCache<Integer, String> cache = new LRUCache<Integer, String>(2);
        System.out.println(cache.get(1));
        cache.put(1, "a");
        System.out.println(cache.get(1));
        cache.put(2, "b");
        System.out.println(cache.get(2));
        System.out.println(cache.get(1));
        cache.put(1, "c");
        System.out.println(cache.get(1));
    }
}
